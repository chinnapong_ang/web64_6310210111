import logo from './logo.svg';
import './App.css';
import TopMenu from './TopMenu';
import Footer from './Footer';

function App() {
  return (
    <div className="App">
      <TopMenu />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Sawasedee Jaaaaa !!!  I am Chinna.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <Footer />
    </div>
  );
}

export default App;
